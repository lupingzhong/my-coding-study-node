# elasticsearch学习入门笔记

### 创建索引

~~~properties
PUT http://localhost:9200/books
响应：
{
    "acknowledged": true,
    "shards_acknowledged": true,
    "index": "books"
}
-------------------------------------
C:\Users\admin\Desktop>curl -XPUT http://localhost:9200/index
{"acknowledged":true,"shards_acknowledged":true,"index":"index"}
~~~

### 创建文档

~~~properties
POST http://localhost:9200/books/book
{
	"author": "庞谷丽",
	"title": "一念天堂,一念地狱",
	"type": "现代小说",
	"tags": "[悬疑,剧情]",
	"publishDate": "2015-11-21"
}
响应：
{
    "_index": "books",
    "_type": "book",
    "_id": "AW4jOLurgjbpYRIolKYV",
    "_version": 1,
    "result": "created",
    "_shards": {
        "total": 2,
        "successful": 1,
        "failed": 0
    },
    "created": true
}
~~~

### 更新文档

~~~properties
PUT http://localhost:9200/books/book/AW4jOLurgjbpYRIolKYV
{
	"author": "庞谷丽",
	"title": "一念天堂,一念地狱",
	"type": "现代小说",
	"tags": "[悬疑,社会]",
	"publishDate": "2016-11-29"
}
{
    "_index": "books",
    "_type": "book",
    "_id": "AW4jOLurgjbpYRIolKYV",
    "_version": 2,
    "result": "updated",
    "_shards": {
        "total": 2,
        "successful": 1,
        "failed": 0
    },
    "created": false
}
~~~

### 查询文档

~~~properties
GET http://localhost:9200/books/book/AW4jOLurgjbpYRIolKYV
C:\Users\admin\Desktop>curl -XGET http://localhost:9200/books/book/AW4jOLurgjbpYRIolKYV
{"_index":"books","_type":"book","_id":"AW4jOLurgjbpYRIolKYV","_version":2,"found":true,"_source":{
        "author": "庞谷丽",
        "title": "一念天堂,一念地狱",
        "type": "现代小说",
        "tags": "[悬疑,社会]",
        "publishDate": "2016-11-29"
}}

如果文档不存在，则返回：
{
    "_index": "books",
    "_type": "book",
    "_id": "AW4jOLurgjbpYRIolKYV",
    "found": false
}
~~~

### 删除文档

~~~properties
DELETE http://localhost:9200/books/book/AW4jOLurgjbpYRIolKYV

{
    "found": true,
    "_index": "books",
    "_type": "book",
    "_id": "AW4jOLurgjbpYRIolKYV",
    "_version": 3,
    "result": "deleted",
    "_shards": {
        "total": 2,
        "successful": 1,
        "failed": 0
    }
}
~~~

### 删除所有文档

~~~properties
删除所有文档：
DELETE http://localhost:9200/_all
{
    "acknowledged": true
}
~~~

### 检索文档

~~~properties
curl -XPOST http://localhost:9200/index/_search  -H 'Content-Type:application/json' -d '{query:{"match":{"title":"地狱"}}}'
curl -XPOST http://localhost:9200/books/book/_search  -H 'Content-Type:application/x-www-form-urlencoded' -d '{query:{"match":{"title":"地狱"}}}'

POST http://localhost:9200/books/book/_search
{
	"query":{
		"match":{
			
			"author":"谷丽"
		}
	}
}
~~~

参考文档：

~~~properties
检索语法：
https://www.elastic.co/guide/cn/elasticsearch/guide/cn/structured-search.html
~~~



### 常见问题

-------------------
1.初次创建索引时，ES服务器无响应问题！
解决：旧的ES环境，可以删除elasticsearch-6.2.1\data\nodes目录以完全清除ES环境。

-----------------------
2.ES数据存储：
E:\server\elasticsearch-5.6.16\data\nodes\0\indices
该目录下的每一个文件夹就是一个索引库，没发送一个创建索引请求，就会自动生成一个目录，目录名称随机生成！

-----------------------



# elasticsearch环境搭建问题

官方文档: https://www.elastic.co/guide/en/elasticsearch/reference/5.6/index.html

本教程使用版本：**elasticsearch-5.6.16.**

### elasticsearch下载及配置

~~~properties
elasticsearch下载及配置
https://artifacts.elastic.co/downloads/elasticsearch
https://www.elastic.co/cn/downloads/elasticsearch
https://www.elastic.co/cn/downloads/past-releases#elasticsearch
E:\server\elasticsearch-5.6.16\bin
D:\installed\Java\jdk1.8.0_152\lib
~~~



### elasticsearch启动错误

~~~properties
E:\server\elasticsearch-5.6.16\bin>elasticsearch.bat
Error occurred during initialization of VM
Could not reserve enough space for 2097152KB object heap
--------------------------------------
由于 elasticsearch5.0 默认分配 jvm 空间大小为2g，修改 jvm空间分配
# vim config/jvm.options  
-Xms2g  
-Xmx2g
修改为
-Xms512m
-Xmx512m
~~~

### 安装中文分词插件

在线安装

~~~properties
cd bin/
elasticsearch-plugin.bat install https://github.com/medcl/elasticsearch-analysis-ik/releases/download/v5.6.16/elasticsearch-analysis-ik-5.6.16.zip
~~~

下载插件安装

~~~properties
源码下载：
https://github.com/medcl/elasticsearch-analysis-ik
-----------------------------------------------------------
由于网络慢，下载不来插件，只能下载源码，就用那个源码安装吧
源码安装，参考博客：
https://www.cnblogs.com/hoojjack/p/7608248.html
~~~



### 遇到问题及解决

#### 下载插件问题

~~~properties
admin@lzph-pc MINGW64 /e/elasticsearch-analysis-ik
$ git clone https://github.com/medcl/elasticsearch-analysis-ik
Cloning into 'elasticsearch-analysis-ik'...
fatal: unable to access 'https://github.com/medcl/elasticsearch-analysis-ik/': error setting certificate verify locations:
  CAfile: D:/installed/Git/mingw64/libexec/ssl/certs/ca-bundle.crt
  CApath: none
----------------
改为如下方式解决：
$ git clone git@github.com:medcl/elasticsearch-analysis-ik.git
~~~

#### 启动报错问题

~~~properties
使用源码安装的方式，安装完后，重启，报错：
[2019-10-31T23:56:17,672][WARN ][o.e.b.ElasticsearchUncaughtExceptionHandler] [] uncaught exception in thread [main]
org.elasticsearch.bootstrap.StartupException: org.elasticsearch.bootstrap.BootstrapException: java.nio.file.NoSuchFileException: E:\server\elasticsearch-5.6.16\plugins\analysis-ik\plugin-descriptor.properties
将plugin-descriptor.properties文件从jar包中拷贝出来，重启，又报错：
E:\server\elasticsearch-5.6.16\bin>elasticsearch.bat
[2019-11-01T00:00:41,401][WARN ][o.e.b.ElasticsearchUncaughtExceptionHandler] [] uncaught exception in thread [main]
org.elasticsearch.bootstrap.StartupException: java.lang.IllegalArgumentException: the version needs to contain major, minor, and revision, and optionally the build: ${elasticsearch.version}

[2019-11-01T00:09:18,317][WARN ][o.e.b.ElasticsearchUncaughtExceptionHandler] [] uncaught exception in thread [main]
org.elasticsearch.bootstrap.StartupException: java.lang.IllegalArgumentException: node settings must not contain any index level settings

[2019-11-01T00:41:35,382][WARN ][o.e.b.ElasticsearchUncaughtExceptionHandler] [] uncaught exception in thread [main]
org.elasticsearch.bootstrap.StartupException: java.lang.ClassCastException: class org.elasticsearch.index.analysis.IkAnalyzerProvider

=======================
解决：
参考博客：https://www.cnblogs.com/yijialong/p/9717593.html
其中重要步骤：修改pom.xml版本（不然就会出现以上错误）
~~~



~~~properties
将elasticSearch安装为window服务：
https://www.cnblogs.com/viaiu/p/5715200.html
~~~



# elasticsearch整合springboot

### 异常1

~~~properties
Field filmRepository in org.hscoder.websearcher.service.EsFilmImporter required a bean named 'elasticsearchTemplate' that could not be found.

The injection point has the following annotations:
	- @org.springframework.beans.factory.annotation.Autowired(required=true)
	
	
原因：没有拷贝EsFilmRepositoryImpl类	
~~~

### 异常2

~~~properties
Description:

Field template in org.hscoder.websearcher.repository.EsFilmRepositoryImpl required a bean of type 'org.springframework.data.elasticsearch.core.ElasticsearchTemplate' that could not be found.

The injection point has the following annotations:
	- @org.springframework.beans.factory.annotation.Autowired(required=true)

The following candidates were found but could not be injected:
	- Bean method 'elasticsearchTemplate' in 'ElasticsearchDataAutoConfiguration' not loaded because @ConditionalOnBean (types: org.elasticsearch.client.Client; SearchStrategy: all) did not find any beans of type org.elasticsearch.client.Client
-------------------------------------
没有添加配置：
#########################################################
#elasticsearch配置
#########################################################
# 启用ElasticsearchDataAutoConfiguration功能
spring.data.elasticsearch.properties.enabled=true
# 集群名称（与启动实例保持相同）
spring.data.elasticsearch.cluster-name=elasticsearch
# 集群监听端口地址（9300端口）
spring.data.elasticsearch.cluster-nodes=127.0.0.1:9300
~~~

### 异常3

~~~properties
Caused by: java.lang.IllegalArgumentException: unknown setting [enabled] please check that any required plugins are installed, or check the breaking changes documentation for removed settings
------------------------------------------------------
配置写错：spring.data.elasticsearch.properties.enabled=true
------------------------------------------------------
正确写法：spring.data.elasticsearch.repositories.enabled=true
~~~

### 异常4

~~~properties
问题描述：引入springfox-swagger2依赖之后启动项目报错：

org.springframework.context.ApplicationContextException: Unable to start web server; nested exception is org.springframework.boot.web.server.WebServerException: Unable to start embedded Tomcat
Caused by: org.springframework.boot.web.server.WebServerException: Unable to start embedded Tomcat
Caused by: org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'formContentFilter' defined in class path resource [org/springframework/boot/autoconfigure/web/servlet/WebMvcAutoConfiguration.class]: Bean instantiation via factory method failed; nested exception is org.springframework.beans.BeanInstantiationException: Failed to instantiate [org.springframework.boot.web.servlet.filter.OrderedFormContentFilter]: Factory method 'formContentFilter' threw exception; nested exception is java.lang.NoClassDefFoundError: Could not initialize class com.fasterxml.jackson.databind.ObjectMapper
Caused by: org.springframework.beans.BeanInstantiationException: Failed to instantiate [org.springframework.boot.web.servlet.filter.OrderedFormContentFilter]: Factory method 'formContentFilter' threw exception; nested exception is java.lang.NoClassDefFoundError: Could not initialize class com.fasterxml.jackson.databind.ObjectMapper
Caused by: java.lang.NoClassDefFoundError: Could not initialize class com.fasterxml.jackson.databind.ObjectMapper
-----------------------------
解决：
参考博客
https://blog.csdn.net/weixin_38342534/article/details/94389823

~~~

### 异常5

~~~properties
问题描述
***************************
APPLICATION FAILED TO START
***************************

Description:

Field template in org.hscoder.websearcher.repository.EsFilmRepositoryImpl required a bean of type 'org.springframework.data.elasticsearch.core.ElasticsearchTemplate' that could not be found.

The injection point has the following annotations:
	- @org.springframework.beans.factory.annotation.Autowired(required=true)

The following candidates were found but could not be injected:
	- Bean method 'elasticsearchTemplate' in 'ElasticsearchDataAutoConfiguration' not loaded because @ConditionalOnBean (types: org.elasticsearch.client.Client; SearchStrategy: all) did not find any beans of type org.elasticsearch.client.Client

Action:

Consider revisiting the entries above or defining a bean of type 'org.springframework.data.elasticsearch.core.ElasticsearchTemplate' in your configuration.

--------------------------------------------------------------------
原因：
	没有开启elasticsearch服务
~~~

~~~properties
================================================================================
2019-11-06 02:06:13.681–[elasticsearch[_client_][transport_client_boss][T#1]] WARN  -org.elasticsearch.transport.TcpTransport.onException(TcpTransport.java:1028) - exception caught on transport layer [[id: 0xf041d8d4, L:/127.0.0.1:60901 - R:/127.0.0.1:9300]], closing connection
java.io.IOException: 远程主机强迫关闭了一个现有的连接。


================================================================================
http://localhost/swagger-ui.html
-----------------------------------------
Unable to infer base url. This is common when using dynamic servlet registration or when the API is behind an API Gateway. The base url is the root of where all the swagger resources are served. For e.g. if the api is available at http://example.org/api/v2/api-docs then the base url is http://example.org/api/. Please enter the location manually: 

================================================================================

~~~



# elasticsearch常用查询

### 检测elasticsearch是否安装成功

~~~properties
http://localhost:9200/
~~~

~~~properties
{
  "name": "Zpzhy6y",
  "cluster_name": "elasticsearch",
  "cluster_uuid": "EwoNCRMvTr-2eczXSxtlRw",
  "version": {
    "number": "5.6.16",
    "build_hash": "3a740d1",
    "build_date": "2019-03-13T15:33:36.565Z",
    "build_snapshot": false,
    "lucene_version": "6.6.1"
  },
  "tagline": "You Know, for Search"
}
~~~





### REST端点查询

~~~properties
http://localhost:9200/_cat/
~~~

结果如下：

~~~properties
=^.^=
/_cat/allocation
/_cat/shards
/_cat/shards/{index}
/_cat/master
/_cat/nodes
/_cat/tasks
/_cat/indices
/_cat/indices/{index}
/_cat/segments
/_cat/segments/{index}
/_cat/count
/_cat/count/{index}
/_cat/recovery
/_cat/recovery/{index}
/_cat/health
/_cat/pending_tasks
/_cat/aliases
/_cat/aliases/{alias}
/_cat/thread_pool
/_cat/thread_pool/{thread_pools}
/_cat/plugins
/_cat/fielddata
/_cat/fielddata/{fields}
/_cat/nodeattrs
/_cat/repositories
/_cat/snapshots/{repository}
/_cat/templates
~~~



### 查询所有索引库

查询当前elasticsearch节点的所有索引库。

http://localhost:9200/_cat/indices

示例结果：

~~~properties
yellow open books    1XdLoYcNTquMkUo8IqBHPA 5 1    4 0 23.3kb 23.3kb
yellow open accounts pBxOgtfDSp-fgkk3bY8aRw 5 1    2 0  7.8kb  7.8kb
yellow open atguigu  05Ss-Lg2R3agRjeyrX1K_Q 5 1    1 0  5.3kb  5.3kb
yellow open dytttags NfCZcHEgSgSyfFpO6ypR6Q 5 1    1 0   22kb   22kb
yellow open dyttfilm Kk88MK8tSamUzc4rfW39ZQ 5 1 2442 0 14.3mb 14.3mb
yellow open weather  J9oXIOLaToSltlCdRQoElA 5 1    0 0   955b   955b
~~~

